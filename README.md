# Semestrální práce z předmětu NI-MVI - Doporučování článků k obrázkům

## [Google Colab](https://colab.research.google.com/drive/1KfN5Ymoc16WwovzBdJAZGBbgPmZ8BkS8?usp=sharing)

## Cíl práce
Na základě vstupního obrázku automaticky vyberte vhodný článek. 
Doporučování článků může probíhat na základě jejich nadpisů či celého obsahu.
Pro trénování a testování používejte články z Wikipedie, které často obsahují ilustrační obrázky.

### Doporučené zdroje
- Extrakce příznaků z textu
    - [Word2Vec](https://www.tensorflow.org/tutorials/text/word2vec)
    - [Char-RNN](https://github.com/karpathy/char-rnn)
    - [Stanford Natural Language Toolkit](https://www.nltk.org/)

## Popis výchozího datasetu
Pro snažší získání potřebných dat jsem se rozhodl využít již předzpracovaný dataset kvalitních článků z Wikipedie. 
[Dataset](https://www.kaggle.com/datasets/jacksoncrow/wikipedia-multimodal-dataset-of-good-articles) je veřejně dostupný na platformě Kaggle (po přihlášení).
Dataset obsahuje 36 476 článků společně s jejich ilustračními obrázky, kterých je celkem 216 463.

Dataset dostupný přímo z Kaggle obsahuje pouze metadata obrázků, jeho kompletní verze i s obrázky je poté dostupná na [Google Drive](https://drive.google.com/file/d/1l0Oyv2Y6LmPGN3lP9MB6i8WWCinqkYPk/view)

### Struktura datasetu
Pro každý článek obsahuje jednu složku pojmenovanou podle nadpisu článku. Ve složce jsou dostupné metainformace v souboru *text.json* 
a dále samotné obrázky (složka *img*) a jejich metainformace (soubor *meta.json*) 
```
.
+-- articleTitle
     +-- text.json
     +-- img
         +-- firstImage
         .
         .
         +-- meta.json
.
.
```

#### Schéma souboru text.json
Příklad uložených dat:
```javascript
{
  "title": "Naval Battle of Guadalcanal",
  "id": 405411,
  "url": "https://en.wikipedia.org/wiki/Naval_Battle_of_Guadalcanal",
  "text": "The Naval Battle of Guadalcanal, sometimes referred to as... "
}
```
Popis atributů:
- title - nadpis článku
- id - unikátní identifikátor článku (stránky, na které se nachází)
- url - URL adresa stránky na Wikipedii
- text - celý obsah článku

#### Schéma souboru meta.json
```javascript
{
  "img_meta": [
    {
      "filename": "d681a3776d93663fc2788e7e469b27d7.jpg",
      "title": "Metallica Damaged Justice Tour.jpg",
      "description": "Metallica en concert",
      "url": "https://en.wikipedia.org/wiki/File%3AMetallica_Damaged_Justice_Tour.jpg",
      "features": [123.23, 10.21, ..., 24.17]
     }
   ]
}
```
Popis atributů:
- filename - unikátní identifikátor obrázku, MD5 hash původního popisu obrázku
- title - popis obrázku získaný z Wikipedia Commons (pokud je dostupný)
- description - krátký popis obrázku
- url - URL adresa obrázku na Wikipedii
- features - význačné znaky extrahované pomocí konvoluční neuronové sítě ResNet152

## Předzpracování datasetu
Pro vypracování semestrální práce jsem se rozhodl využít framework [PyTorch](https://pytorch.org/).
Ten pro práci s datasety nabízí pomocné třídy [Dataset](https://pytorch.org/docs/stable/data.html#torch.utils.data.Dataset)
a [DataLoader](https://pytorch.org/docs/stable/data.html#torch.utils.data.DataLoader).

Definoval jsem vlastní třídu WipediaArticlesDataset, která dědí z již zmíněné třídy Dataset. Ta načte data článků ve formátu uvedeném výše z předaného kořenového adresáře.
Je třeba projít všechny podsložky, abychom zjistili, kolik obrázků a tedy trénovacích či testovacích vstupů je dostupných.

Nakonec jsem připravený dataset rozdělil na dvě části v poměru 4/5 : 1/5 pro trénovací a testovací část. 
Pro náhodné rozdělení lze použít metodu [random_split](https://pytorch.org/docs/stable/data.html#torch.utils.data.random_split) z PyTorch.

### Předzpracování obrázků

Samotné načítání obrázků probíhá až při přístupu ke konkrétní položce datasetu a to pomocí [PIL image modulu](https://pillow.readthedocs.io/en/stable/reference/Image.html).
Použití tohoto modulu má hned dvě výhody. Nabízí snadnou možnost konverze do RGB (potřebujeme, aby všechny vstupní obrázky měly stejný počet kanálů) a většina
[transformerů](https://pytorch.org/vision/stable/transforms.html) z PyTorch podporuje PIL obrázky na vstupu.

Všechny obrázky bylo třeba převést na stejnou velikost. Konkrétně jsem se rozhodl pro 256 X 256 pixelů a provedl jsem to s pomocí již zmíněných transformerů.
Dále je vhodné provést normalizaci, která zajišťuje, že každý vstup (v našem případě pixel) má podobnou distribuci hodnot. 
To urychluje konvergenci při trénování sítě
[[Zdroj](https://becominghuman.ai/image-data-pre-processing-for-neural-networks-498289068258)].

### Extrakce příznaků z textu

Při analýze jsem vyšel z doporučené literatury, ale zkoumal jsem i další možnosti. 
Hezký přehled způsobů, jak získat vektorovou reprezentaci pro celou větu je dostupný [zde](https://www.baeldung.com/cs/sentence-vectors-word2vec).

Analyzoval jsem tyto možnosti extrakce:
- [Word2Vec](https://www.tensorflow.org/tutorials/text/word2vec) - získám vektorovou reprezentaci pro jednotlivá slova. Alternativní [zdroj](https://towardsdatascience.com/word2vec-with-pytorch-implementing-original-paper-2cd7040120b0).
- [Doc2Vec](https://heartbeat.comet.ml/getting-started-with-doc2vec-2645e3e9f137) - získám vektorovou reprezentaci pro celý dokument.
- [SentenceTransformers](https://www.sbert.net/index.html) - získám vektorovou reprezentaci pro jednotlivé věty.

Pro doporučování článků jsem se v první verzi rozhodl použít jejich nadpisy. Ty jsou tvořené více slovy, proto není možné jednoduše využít Word2Vec model.
Po konzultaci jsem vybral Sentence Transformery s tím, že použiji již natrénovaný model. 
Budu mít tedy více času se zaměřit na samotnou konvoluční neuronovou síť pro extrakci příznaků z obrázku.

#### Volba předtrénovaného modelu

Po prozkoumání dostupných předtrénovaných [modelů](https://www.sbert.net/docs/pretrained_models.html#sentence-embedding-models/) jsem se rozhodl využít model
*all-MiniLM-L6-v2*. Ten má rozumnou velikost (přesně 80 MB) a umožňuje zpracovat relativně velké množství vět za sekundu při zachování velmi dobré kvality výstupu.
Umožňuje zpracovávat věty až do délky 256 slov a jeho výstupem je vektor o 384 dimenzích.

## Navržený model

Pro extrakci příznaků z textu je vhodné použít konvoluční neuronovou síť ([CNN](https://en.wikipedia.org/wiki/Convolutional_neural_network)).
Při jejím návrhu jsem se inspiroval [AlexNetem](https://en.wikipedia.org/wiki/AlexNet).
Jedná se o síť, která v roce 2012 vyhrála [ImageNet výzvu](https://en.wikipedia.org/wiki/ImageNet#ImageNet_Challenge).
Na vstupu přijímá obrázky velikosti 224 X 224 pixelů a má rozumnou hloubku. 

Hloubka modelu je jedním z hlavních parametrů úspěchu. Aby síť produkovala dostatečně spolehlivé výsledky, je třeba, aby měla aspoň určitou hloubku.
Zároveň avšak existuje určitá hranice, po které dochází ke zhoršení výsledků [[Zdroj](https://openreview.net/references/pdf?id=u6JCN9ny8)].
Při větší hloubce modelu je také třeba větší výpočetní výkon. 

Dále jsem čepral z [přednášky](https://courses.fit.cvut.cz/NI-MVI/@B211/media/lectures/03/NI-MVI-convnet.pdf) o Konvolučních neuronových sítí a autoencoders.
Na slidu 37 je vidět přehled možných architektur konvolučních sítí.

Výsledkem je model s 23 skrytými vrstvami, konkrétně obsahuje 8 konvolučních, 9 [ReLU](https://en.wikipedia.org/wiki/Rectifier_(neural_networks)),
4 poolovací a 2 fully connected vrstvy. Všechny poolovací vrstvy provádí max pooling s velikostí kernelu 2 a posunem také 2.

Výslednou architekturu lze zjednodušeně zachytit následovně:
```
INPUT -> [Conv -> ReLU -> Conv -> ReLu -> Pool]*4 -> FC -> ReLU -> FC -> OUTPUT
```

## Trénování modelu

Trénování jsem prováděl po batchích o velikosti 256. Větší batche znamenají větší chybovost modelu, ovšem trénování sítě trvá kratší dobu
[[Zdroj]](https://wandb.ai/ayush-thakur/dl-question-bank/reports/What-s-the-Optimal-Batch-Size-to-Train-a-Neural-Network---VmlldzoyMDkyNDU).
Vzhledem k omezenému výkonu, který je dostupný v Google Colab ve verzi zdarma, je tato velikost batche rozumným kompromisem mezi doubou trénovaní a výslednou chybovostí.

Při trénování chceme minimalizovat vzdálenost výstupu sítě od původního nadpisu článku, ke kterému obrázek patří. 
K tomuto se nám hodí ztrátová funkce [sum of squares](https://en.wikipedia.org/wiki/Residual_sum_of_squares) - suma čtvterců, 
která je pouze drobnou úpravou velmi používané ztrátové funkce [mean squared error](https://en.wikipedia.org/wiki/Mean_squared_error) - střední kvadratická chyba.
Lze tedy použít metodu [MSELoss](https://pytorch.org/docs/stable/generated/torch.nn.MSELoss.html) z PyTorch s parametrem reduction rovným hodnotě 'sum'.

Jako optimizátor jsem se rozhodl použít AdamW. Ten je rozšířenou verzí optimizátoru Adam, ovšem ve většině případů dosahuje menší chybovosti 
a modely jsou lépe generalizované [[Zdroj]](https://www.fast.ai/posts/2018-07-02-adam-weight-decay.html).

### Normalizace

Zvolil jsem dvě normalizační techniky:

#### [Batch normalization](https://en.wikipedia.org/wiki/Batch_normalization)

Parametry sítě se neupravují na základě jednoho vstupu, ale více vstupů najednou.
To urychluje a zlepšuje konvergenci při trénování neuronových sítí. Zamezuje to totiž tomu, aby došlo k velké změně parametrů na základě jednoho vstupu,
který však příliš nevypovídá o zbytku datasetu. 

Tuto normalizaci jsem využil jak v konvoluční, tak fully connected části sítě.

#### [Dropout](https://en.wikipedia.org/wiki/Dilution_(neural_networks))
Zprůměrováním výstupu více modelů dostanu lepší výsledky, ovšem trénování více neuronových sítí je drahé. Touto technikou to lze efektivně simulovat.
Při trénování náhodně vyberu určitý počet neuronů, jejichž výstup ignoruji. Rozhodování sítě se tak nemůže stát příliš závislé pouze na některých neuronech. 

Dropout provádím pouze ve fully connected vrstvě a to s pravděpodobností 20%.

## Dosažené výsledky

Model se zatím naučil predikovat vektorovou reprezentaci nadpisů článků s určitou přesností. 
Nejlepší dosažená trénovací ztráta byla 216.44 a jí odpovídající validační ztráta 222.07, přes celý batch. 
To odpovídá průměrné trénovací ztrátě 0.85 a validační ztrátě 0.87 na jeden vstup při velikosti batche 256.

Kvalitu modelu jsem také vyhodnocoval subjektivně, nalezením 10 článků, které jsou nejpodobnější výstupu modelu.
Vzal jsem vektorové reprezentace všech článků v databázi a spočítal jsem jejich [euklidovskou vzdálenost](https://en.wikipedia.org/wiki/Euclidean_distance) od výstupu sítě.
Poté jsem nalezl 10 nejbližších sousedů - článků, jejichž euklidovská vzdálenost je nejmenší.

Pro některé obrázky model doporučuje vhodné články, pro některé jiné příliš obecné či zcela nerelevantní.
Například má problém s rozpoznáváním jednotlivých osob a tak téměř pro každý obrázek, který obsahuje fotku libovolné osoby, doporučuje jednu danou osobu. 